const express = require('express');
const bodyParser = require('body-parser');
const mongoose  = require('mongoose');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const path = require('path');

const app = express();

mongoose.Promise  = global.Promise;

//Mongodb connection
mongoose.connect('mongodb://127.0.0.1:27017/vivo',{ useNewUrlParser: true });
mongoose.connection.once('open', function() {
	console.log("Database connected successfully");
});

//To enable Cross-Origin Resource Sharing
app.use(cors());

// default options
app.use(fileUpload());

//BodyParser
app.use(bodyParser.json());

//Route
app.use('/api',require('./routes/api'));


//Error Handling
app.use(function(err,req,res,next){
  //console.log(err);
  res.status(422).send({error:err.message});
});

app.use(express.static(path.join(__dirname, 'build')));

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

//Port listen
let port = 3010;
app.listen(process.env.port || port, function(){
  console.log("Port Listening "+port);
});
