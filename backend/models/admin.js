const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
const moment = require('moment');


const adminSchema = new Schema({
  date: { type : Date , default:moment()},
  username:String,
  password:String
});

//Model
const admin = mongoose.model('admin',adminSchema);


module.exports    = admin;