const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
const moment = require('moment');


const requestSchema = new Schema({
  date: { type : Date , default:moment()},
  username:String,
  reason:String,
  comments:String,
  status:String
});

//Model
const request = mongoose.model('service_request',requestSchema);


module.exports    = request;
