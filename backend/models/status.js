const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
const moment = require('moment');


const statusSchema = new Schema({
  date: { type : Date , default:moment()},
  status:String
});

//Model
const status = mongoose.model('status',statusSchema);


module.exports    = status;
