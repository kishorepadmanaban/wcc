const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
const moment = require('moment');


const userSchema = new Schema({
  date: { type : Date , default:moment()},
  username:String,
  password:String
});

//Model
const user = mongoose.model('user',userSchema);


module.exports    = user;
