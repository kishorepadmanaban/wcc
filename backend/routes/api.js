const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const moment = require('moment-timezone');
const path = require('path');
const axios = require('axios');
const fs = require('fs');
const PDFDocument = require ('pdfkit');

const Request = require('../models/request');
const Status = require('../models/status');
const User = require('../models/user');
const Admin = require('../models/admin');


//Post Service Request
router.post('/service_request', function(req, res, next) {
    Request.create(req.body).then(function(data) {
    res.send(data);
    }).catch(next);
});


//Get Service Requests
router.get('/service_request', function(req, res, next) {
    Request.find({
        date: {
        $lt: req.query.todate,
        $gte: req.query.fromdate
      }
    },null,{sort:{date:-1}}).then(function(data) {
        res.send(data);
    }).catch(next);
});

//Post Service Engineer Status
router.post('/status', function(req, res, next) {
    Status.create(req.body).then(function(data) {
      res.send(data);
    }).catch(next);
});

//Get Service Engineer Status
router.get('/status', function(req, res, next) {
    Status.findOne({},null,{sort: {$natural:-1}}).then(function(data) {
        res.send(data);
    }).catch(next);
});

//Update Service Status
router.put('/service_request_status/:id', function(req, res, next) {
    Request.findByIdAndUpdate({_id:req.params.id},{status:req.body.status}).then(function(data) {
        Request.find({},null,{sort:{date:-1}}).then(function(data){
            res.send(data);
        })
    }).catch(next);
});

//Post Service Request
router.post('/user', function(req, res, next) {
    User.findOne({username:req.body.username,password:req.body.password}).then(function(data){
        if(!data)
        {
            User.create(req.body).then(function(data) {
                User.find({},null,{sort:{date:-1}}).then(function(data) {
                    res.send(data);
                });
            })
        }
    }).catch(next);
});


//Get Service Requests
router.get('/user', function(req, res, next) {
    User.find({},null,{sort:{date:-1}}).then(function(data) {
        res.send(data);
    }).catch(next);
});

//Post Service Request
router.delete('/user/:id', function(req, res, next) {
    User.findByIdAndRemove({_id:req.params.id}).then(function(data) {
        User.find({},null,{sort:{date:-1}}).then(function(data) {
            res.send(data);
        });
    }).catch(next);
});


//User login
router.post('/login', function(req, res, next) {
    User.findOne({username:req.body.username,password:req.body.password}).then(function(data) {
        if(data){
            res.send({status:"success",username:data.username})
        }
    }).catch(next);
});

//Admin login
router.post('/adminlogin', function(req, res, next) {
    Admin.findOne({username:req.body.username,password:req.body.password}).then(function(data) {
        if(data){
            res.send({status:"success",username:data.username})
        }
    }).catch(next);
});

//Post Admin
router.post('/postadminlogin', function(req, res, next) {
    Admin.create(req.body).then(function(data) {
        res.send(data);
    }).catch(next);
});





module.exports = router;
